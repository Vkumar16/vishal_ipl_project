// 9. Find the bowler with the best economy in super overs
function bowlerWithTheBestWEonomyInSuperOver(deliveries) {

    if (Array.isArray(deliveries) == true && deliveries.length > 0) {

        const superOverData = deliveries.filter(currentValue => currentValue.is_super_over != '0')

        const bowlerNameAndTotalRunInSuperOver = superOverData.reduce((accum, currentValue) => {
            if (currentValue.bowler in accum) {
                accum[currentValue.bowler][0] += Number(currentValue.total_runs)
                accum[currentValue.bowler][1] += 1

            } else {
                accum[currentValue.bowler] = []
                accum[currentValue.bowler].push(Number(currentValue.total_runs))
                accum[currentValue.bowler].push(1)
            }
            return accum

        }, {})

        const bowlerWithTheBestEconomyInSuperOverData = []

        for (let bowlerName in bowlerNameAndTotalRunInSuperOver) {

            let object = {}
            object.bowler = bowlerName
            object.economy = (bowlerNameAndTotalRunInSuperOver[bowlerName][0] / bowlerNameAndTotalRunInSuperOver[bowlerName][1]).toFixed(3)
            
            bowlerWithTheBestEconomyInSuperOverData.push(object)
        }

        return bowlerWithTheBestEconomyInSuperOverData
    }
    return []
}


module.exports = bowlerWithTheBestWEonomyInSuperOver