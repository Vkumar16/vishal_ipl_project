// 2. Number of matches won per team per year in IPL.

function matchesWonPerTeamPerYear(matches) {

    if (Array.isArray(matches)===true && matches.length != 0) {
        let dataMatcesWonPerTeamPerYear = matches.reduce((accum, currentValue) => {
            if (currentValue.season in accum) {
                if (currentValue.winner in accum[currentValue.season]) {
                    accum[currentValue.season][currentValue.winner]++
                }
                else {
                    if (currentValue.winner.length > 0) {
                        accum[currentValue.season][currentValue.winner] = 1
                    }
                }
            }
            else {
                accum[currentValue.season] = {}

                if (currentValue.winner.length > 0) {
                    accum[currentValue.season][currentValue.winner] = 1
                }
            }
            return accum


        }, {})

        return dataMatcesWonPerTeamPerYear
    }

}

module.exports = matchesWonPerTeamPerYear