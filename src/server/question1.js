function matchesPlayedPerYear(matches) {

    if (Array.isArray(matches )=== true && matches.length != 0) {

        let dataMatchesPlayedPerYear = matches.reduce((accum, currentValue) => {
            const year = currentValue.season

            if (year in accum) {
                accum[currentValue.season]++
            }
            else {
                accum[currentValue.season] = 1
            }

            return accum
            
        }, {})

        let result = []

        for (let year in dataMatchesPlayedPerYear) {
            result.push({ year, numberOfmatchesPlayed: dataMatchesPlayedPerYear[year] })
        }

        return result
    }
    return []
}

module.exports = matchesPlayedPerYear