
// 6. Find a player who has won the highest number of Player of the Match awards for each season
function playerOfTheMatch(matches) {

    if (Array.isArray(matches) === true && matches.length > 0) {

        const playerOfTheMatchData = matches.reduce((accum, currentValue) => {

            let playerName = currentValue.player_of_match

            if (playerName.length > 0) {
                if (currentValue.season in accum) {

                    if (currentValue.player_of_match in accum[currentValue.season]) {
                        accum[currentValue.season][playerName]++
                    }
                    else {
                        accum[currentValue.season][playerName] = 1
                    }

                } else {
                    accum[currentValue.season] = {}
                    accum[currentValue.season][playerName] = 1
                }
            }
            return accum

        }, {})
        const result = {}
        
        for (let season in playerOfTheMatchData) {

            let value = Object.values(playerOfTheMatchData[season]);
            let max = Math.max(...value)

            for (let name in playerOfTheMatchData[season]) {
                if (playerOfTheMatchData[season][name] == max) {
                    if (season in result) {
                        result[season].push({ player: name, playerOfTheMatch: max })
                    }
                    else {
                        result[season] = []
                        result[season].push({ player: name, playerOfTheMatch: max })
                    }
                }
            }

        }
        return result
    }
    return {}
}


module.exports = playerOfTheMatch