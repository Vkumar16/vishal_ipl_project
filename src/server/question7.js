// 7. Find the strike rate of a batsman for each season

function strikeRateOfBatsmanEachSeason(matches, deliveries) {
    if (Array.isArray(matches) === true && Array.isArray(deliveries) === true && matches.length > 0 && deliveries.length > 0) {
        
        let seasonNameRunsBalls = {}

        matches.forEach(currentValue => {

            const matchID = currentValue.id
            const season = currentValue.season

            seasonNameRunsBalls = deliveries.reduce((accum, currentValue) => {

                if (matchID == currentValue.match_id) {

                    let batsmanName = currentValue.batsman
                    let batsmanRun = Number(currentValue.batsman_runs)

                    if (season in accum) {
                        if (batsmanName in accum[season]) {
                            accum[season][batsmanName][0] += batsmanRun
                            accum[season][batsmanName][1] += 1;
                        } else {
                            accum[season][batsmanName] = [batsmanRun, 1]
                        }
                    } else {
                        accum[season] = {}
                        accum[season][batsmanName] = [batsmanRun, 1]
                    }
                }
                return accum
            }, seasonNameRunsBalls)

        })
        // seasonNameRunsBalls; = 2017{'LMP Simmons': [ 137, 116 ], [TotalRun , totalBallsFased],...}

        const strikeRateOfBatsmanEachSeasonData = {}

        for (let season in seasonNameRunsBalls) {

            strikeRateOfBatsmanEachSeasonData[season] = []

            for (let name in seasonNameRunsBalls[season]) {

                let totalRuns = seasonNameRunsBalls[season][name][0]
                let ballFaced = seasonNameRunsBalls[season][name][1]
                let object = {}
                object.batsman = name
                object.strikeRate = ((totalRuns / ballFaced) * 100).toFixed(3)
                
                strikeRateOfBatsmanEachSeasonData[season].push(object)
            }
        }
        return strikeRateOfBatsmanEachSeasonData
    } return {}
}

module.exports = strikeRateOfBatsmanEachSeason

