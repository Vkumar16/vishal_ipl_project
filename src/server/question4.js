// 4. Top 10 economical bowlers in the year 2015 
function topTenEconomicalBowlers(matches, deliveries) {

    // first finding all  match id which played in 2015
    if (Array.isArray(matches) === true && Array.isArray(deliveries) === true && matches.length > 0 && deliveries.length > 0) {

        const allMatchID2015 = matches.reduce((accum, currentValue) => {
            if (currentValue.season == 2016) {
                accum.push(currentValue.id)
            }
            return accum;
        }, [])

        
        let data = deliveries.reduce((accum, currentValue) => {

            if (allMatchID2015.indexOf(currentValue.match_id) != -1) {

                if (currentValue.bowler in accum) {
                    accum[currentValue.bowler].nunberOfBalls++;
                    accum[currentValue.bowler].totalRuns += Number(currentValue.total_runs);

                }
                else {
                    accum[currentValue.bowler] = { nunberOfBalls: 1, totalRuns: Number(currentValue.total_runs) }
                }
            }
            return accum
        }, {})


        let bowlerAndEcono = []

        for (let key in data) {
            const economic = (data[key].totalRuns / data[key].nunberOfBalls).toFixed(3)
            bowlerAndEcono.push([key, economic])
        }
        // Now sort bowlerAndEcono in desc order 
        bowlerAndEcono.sort((a, b) => a[1] - b[1])

        bowlerAndEcono = bowlerAndEcono.slice(0, 10)
        let result = []
        // modifying the output so that it looks better and more undersatndable
        for (let value of bowlerAndEcono) {
            result.push({
                bowler: value[0],
                economic: value[1]
            })
        }
        return result
    }
    return []
}

module.exports = topTenEconomicalBowlers