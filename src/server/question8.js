
// 8. Find the highest number of times one player has been dismissed by another player
function playerDismissedByAnotherPlayer(deliveries) {
    if (Array.isArray(deliveries) && deliveries.length > 0) {

        let playerDismissedByAnotherPlayerData = deliveries.filter(currentValue => currentValue.fielder.length > 0)

        playerDismissedByAnotherPlayerData = playerDismissedByAnotherPlayerData.reduce((accum, currentValue) => {
            let playerName = currentValue.player_dismissed
            if (playerName in accum) {
                accum[playerName]++
            }
            else {
                accum[playerName] = 1
            }

            return accum

        }, {})

        let result = []

        for (let name in playerDismissedByAnotherPlayerData) {
            let object = {}
            object.batsman = name
            object.dismissedByAnoterPlayer = playerDismissedByAnotherPlayerData[name]
            result.push(object)

        }
        return result
    }

    return []
}

module.exports = playerDismissedByAnotherPlayer