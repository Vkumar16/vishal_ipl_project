// 5. Find the number of times each team won the toss and also won the match
function teamOwnTheToseAndMatch(matches) {

    if (Array.isArray(matches) == true && matches.length > 0) {

        const teamOwnTheToseData = matches.reduce((accum, currentValue) => {

            if (currentValue.team1 in accum) {
                if (currentValue.team1 === currentValue.winner) {
                    accum[currentValue.team1].matchWins++
                }
                if (currentValue.team1 === currentValue.toss_winner) {
                    accum[currentValue.team1].tossWins++
                }
            }
            else {
                accum[currentValue.team1] = { tossWins: 0, matchWins: 0 }
                if (currentValue.team1 === currentValue.winner) {
                    accum[currentValue.team1].matchWins = 1
                }
                if (currentValue.team1 === currentValue.toss_winner) {
                    accum[currentValue.team1].tossWins = 1
                }
            }

            if (currentValue.team2 in accum) {
                if (currentValue.team2 === currentValue.winner)
                    accum[currentValue.team2].matchWins++
                if (currentValue.team2 === currentValue.toss_winner)
                    accum[currentValue.team2].tossWins++
            }
            else {
                accum[currentValue.team2] = { tossWins: 0, matchWins: 0 }
                if (currentValue.team2 === currentValue.winner)
                    accum[currentValue.team2].matchWins = 1
                if (currentValue.team2 === currentValue.toss_winner)
                    accum[currentValue.team2].tossWins = 1
            }

            return accum

        }, {})

        return teamOwnTheToseData
    }
    
    return {}
}

module.exports = teamOwnTheToseAndMatch