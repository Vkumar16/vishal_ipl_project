const fs = require('fs')
const csvtojson = require('csvtojson')

const matchesPlayedPerYear = require('./question1')
const matchesWonPerTeamPerYear = require('./question2')
const extraRunConcededPerTeam2016 = require('./question3')
const topTenEconomicalBowlers = require('./question4')
const teamOwnTheToseAndMatch = require('./question5')
const playerOfTheMatch = require('./question6')
const strikeRateOfBatsmanEachSeason = require('./question7')
const playerDismissedByAnotherPlayer = require('./question8')
const bowlerWithTheBestWEonomyInSuperOver = require('./question9')



csvtojson().fromFile('../data/matches.csv')
    .then(matches => {
        csvtojson().fromFile('../data/deliveries.csv')
            .then(deliveries => {

                // 1. Number of matches played per year for all the years in IPL.
                const matchesPlayedPerYearData = matchesPlayedPerYear(matches)

                fs.writeFile('../public/output/matchesPlayedPerYearData.json', JSON.stringify(matchesPlayedPerYearData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("matchesPlayedPerYearData saved !")
                    }
                })

                // 2. Number of matches won per team per year in IPL.

                const matchesWonPerTeamPerYearData = matchesWonPerTeamPerYear(matches)

                fs.writeFile('../public/output/matchesWonPerTeamPerYearData.json', JSON.stringify(matchesWonPerTeamPerYearData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("matchesWonPerTeamPerYear data saved !")
                    }
                })

                // 3. Extra runs conceded per team in the year 2016
                const extraRunConcededPerTeam2016Data = extraRunConcededPerTeam2016(matches, deliveries)
                
                fs.writeFile('../public/output/extraRunConcededPerTeam2016Data.json', JSON.stringify(extraRunConcededPerTeam2016Data), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("extraRunConcededPerTeam2016Data.json  saved !")
                    }
                })

                // 4. Top 10 economical bowlers in the year 2015
                const topTenEconomicalBowlersData = topTenEconomicalBowlers(matches, deliveries)

                fs.writeFile('../public/output/topTenEconomicalBowlersData.json', JSON.stringify(topTenEconomicalBowlersData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("topTenEconomicalBowlersData.json file saved !")
                    }
                })


                // 5. Find the number of times each team won the toss and also won the match
                const teamOwnTheToseAndMatchData = teamOwnTheToseAndMatch(matches)

                fs.writeFile('../public/output/teamOwnTheToseAndMatchData.json', JSON.stringify(teamOwnTheToseAndMatchData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("teamOwnTheToseAndMatchData.json file saved !")
                    }
                })

                // 6. Find a player who has won the highest number of Player of the Match awards for each season

                const higestNumberOfPlayerOfTheMatchEachSeason = playerOfTheMatch(matches)

                fs.writeFile('../public/output/higestNumberOfPlayerOfTheMatchEachSeasonData.json', JSON.stringify(higestNumberOfPlayerOfTheMatchEachSeason), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("higestNumberOfPlayerOfTheMatchEachSeasonData.json file saved !")
                    }
                })

                // 7. Find the strike rate of a batsman for each season 
                const strikeRateOfBatsmanEachSeasonData = strikeRateOfBatsmanEachSeason(matches, deliveries)
                
                fs.writeFile('../public/output/strikeRateOfBatsmanEachSeasonData.json', JSON.stringify(strikeRateOfBatsmanEachSeasonData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        console.log("strikeRateOfBatsmanEachSeasonData.json file saved !")
                    }
                })

                // 8. Find the highest number of times one player has been dismissed by another player 
                const playerDismissedByAnotherPlayerData = playerDismissedByAnotherPlayer(deliveries)

                fs.writeFile('../public/output/playerDismissedByAnotherPlayerData.json', JSON.stringify(playerDismissedByAnotherPlayerData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else { console.log("playerDismissedByAnotherPlayerData.json file saved !") }

                })


                // 9. Find the bowler with the best economy in super overs
                const bowlerWithTheBestWEonomyInSuperOverData = bowlerWithTheBestWEonomyInSuperOver(deliveries)
                
                fs.writeFile('../public/output/bowlerWithTheBestWEonomyInSuperOverData.json', JSON.stringify(bowlerWithTheBestWEonomyInSuperOverData), error => {
                    if (error) {
                        console.log(error)
                    }
                    else { console.log("bowlerWithTheBestWEonomyInSuperOverData.json file saved !") }

                })
            })

    })
