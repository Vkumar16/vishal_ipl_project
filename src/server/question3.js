// 3. Extra runs conceded per team in the year 2016

function extraRunConcededPerTeam2016(matches, deliveries) {
    // first finding all  match id which played in 2016
    if (Array.isArray(matches) === true && Array.isArray(deliveries) === true && matches.length > 0 && deliveries.length > 0) {

        const allMatchID2016 = matches.reduce((accum, currentValue) => {

            if (currentValue.season == 2016) {
                accum.push(currentValue.id)
            }
            return accum;

        }, [])


        let data = deliveries.reduce((accum, currentValue) => {

            if (allMatchID2016.indexOf(currentValue.match_id)) {

                if (currentValue.bowling_team in accum) {
                    accum[currentValue.bowling_team] += Number(currentValue.extra_runs)
                }
                else {
                    accum[currentValue.bowling_team] = Number(currentValue.extra_runs)
                }
            }

            return accum

        }, {})
        // modifying the output so that it looks better and more undersatndable
        let result = []
        for (team in data) {
            result.push({ team, extraRunConcededIn2016: data[team] })

        }

        return result
    }

    return []

}
module.exports = extraRunConcededPerTeam2016;
